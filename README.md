# tong3d

#### 项目介绍
大瞳引擎设计的初衷只为让开发人员在开发Webgl 3D乃至未来完善2D的过程中更加方便快捷，能不能解决实际问
题是我们的最终目的。它免下载与安装，引擎在2017年7月份发布1.0版本，发布后发现并没法解决大部分实际的
开发问题！对我们来说，用户既能够参与进来一起完善引擎的研发，又能够满足自身项目功能的开发显得非常重要。
之后我们深化重构了引擎的核心架构，以组件化的开发思想发布了2.0版本，现在用户可以通过http://tong3d.com
进入开发环境！引擎支持物理系统，脚本编写，动画编辑系统，天空盒，视频贴图，音频播放，着色器编写，铰链，
Flare，3D文字，OnGUI绘制,多场景管理等功能。它必然有不足，但是我们在努力做好每一个功能，希望它能帮到你。
渲染系统基于three.js, 物理系统基于cannon.js。  
引擎开发文档地址为：http://docs.tong3d.com/engine/pro/cn/guide-index.html  
技术交流可加QQ群570757436  
大瞳引擎编辑器分别由工程管理面板，场景层级管理面板,菜单栏，资源管理面板，场景编辑器，场景工具栏，代码编辑器，动画编辑器，
属性管理面板，设置面板，场景分析工具，控制台输出面板等组成。
![Image text](http://p2zwa66ps.bkt.clouddn.com/tong_editor.png)  
####动画编辑器:  
动画编辑器通过选取场景中的一个对象，对其对象自身的属性或组件进行插值的编辑，使其产生动画效果,要打开动画编辑器，需要在资源面板tab页右侧的'+'进行开启,编辑某个3D物体过程中,会在
该对象上自动添加动画组件。具体操作可查看[动画编辑器](engine-ani-editor-index.md) 
![image](http://p2zwa66ps.bkt.clouddn.com/ani_editor.gif)   
####物理系统:  
用户可以在物体上添加物理效果（重力，碰撞或约束），以满足游戏或3D项目中的要求。  
![image](http://p2zwa66ps.bkt.clouddn.com/joint_com.gif)  
####资源管理面板:  
该面板管理所有开发过程中需要用到的资源，包括了图片，音视频，模型，材质，js脚本，glsl脚本等。  
![Image text](http://p2zwa66ps.bkt.clouddn.com/assets.png)  
####场景编辑器:  
用户可以在场景编辑器中对3D或2D物体进行位移，旋转，缩放等操作。具体操作可看[3D世界](engine-3d-world-index.md)  
![Image text](http://p2zwa66ps.bkt.clouddn.com/viewport.png)  
####场景工具栏:  
工具栏提供了操作场景编辑器中的一些工具，如位移旋转，空间坐标变换，网格的显示,是否全屏以及播放测试等。  
![Image text](http://p2zwa66ps.bkt.clouddn.com/toolbar.png)  
####代码编辑器:  
代码编辑器提供了js或glsl脚本的编写，其中js每编写完成后的脚本将可以作为一个物体的组件，打开方式通过双击脚本文件,具体操作可看[脚本编写](engine-code-write-index.md)  
![Image text](http://p2zwa66ps.bkt.clouddn.com/script.png)  
####属性管理面板:  
当选择场景中的某个对象时,属性面板就会显示该对象上的可编辑属性和挂载在该对象上的组件等。  
![Image text](http://p2zwa66ps.bkt.clouddn.com/attribute.png)  
####设置面板:  
选择工具栏面板中的设置按钮后，可以弹出设置面板，该面板可以设置物理系统的重力系数，背景颜色,物化效果等。  
![Image text](http://p2zwa66ps.bkt.clouddn.com/Settings.png)  
####场景分析工具:  
分析工具可以对场景的运行效率(帧率), 场景的面数，顶点数等进行分析。打开方式也是通过tab页右侧的'+'按钮进行打开,具体操作可查看[Profile面板](engine-profile-introduce-index.md)   
![Image text](http://p2zwa66ps.bkt.clouddn.com/profile.png)  
####控制台输出面板:  
控制台面板会显示脚本中的console.log,warm,error等信息,便于开发调试。具体操作可查看[控制台面板](engine-console-index.md)  
![Image text](http://p2zwa66ps.bkt.clouddn.com/console.png)  